# FP requirements

**FAQ.json**  
`Question and Answers`

**Positions.json**  
`Title, Location, Overview, and Responsibilities`

**Stores.json (Store's presence on register store page)**  
`Image (URL), Type (small or big), Short Address, Side (left or right)`

**RegisterShops.json**  
`Why shops should register with faripe, and next steps for shop after registration`

**Policies.json**  
`No Idea!`

**Terms.json**  
`No Idea!`

**Career Page Background Image**  
`High quality image required`

**Lead Delivery Banner Graphic**  
`Thematic graphic required`

**Landing Page Graphic**  
`Thematic graphic required`

**White Board Product Animation**  
`An animation of products' USP`